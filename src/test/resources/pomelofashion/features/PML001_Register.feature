@pomelo
Feature: Register

  Scenario Outline: As a new customer, I want to register to Pomelo with my credentials
    Given I am at Pomelo homepage
    And I click on Register button
    Then The Register form should be displayed
    When I input random email
    And I input <firstName> on field firstName
    And I input <lastName> on field lastName
    And I input <password> on field password
    And I click on Create an Account button
    Then I should see a welcome message as Welcome <firstName>

    Examples: 
      | firstName | lastName | password       |
      | Linh      | Do       | Password@12345 |