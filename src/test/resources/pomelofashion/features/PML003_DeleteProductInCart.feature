@pomelo
Feature: Shopping Cart

  @deletecart
  Scenario Outline: Delete product from cart
    Given I am at Pomelo homepage
    And I log in Pomelo with <email> and <password>
    When I search for product with name <productName>
    And I click on product <productName>
    And I click on <size> button
    And I click on Add To Bag button
    And I click on View My Shopping Bag button
    And I delete product <productName> in cart
    Then I should see <productName> is not present

    Examples: 
      | email               | password       | productName                              | size |
      | test121998@mail.com | Password@12345 | G-Bomb Long Sleeve UV Surf Suit - Maroon | XS   |
