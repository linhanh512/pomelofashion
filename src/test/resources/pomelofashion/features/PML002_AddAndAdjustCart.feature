@pomelo
Feature: Shopping Cart

  @adjustcart
  Scenario Outline: Add, adjust and checkout product in cart
    Given I am at Pomelo homepage
    And I log in Pomelo with <email> and <password>
    When I search for product with name <productName>
    And I click on product <productName>
    And I click on <size> button
    #Add to cart
    And I click on Add To Bag button
    Then I should see a message as Item added to Bag
    When I click on View My Shopping Bag button
    And I observe current cart total
    #Adjust product item size
    When I select option M in dropdown Size
    Then I should see the total price is unchanged
    #Adjust product item quantity
    When I select option 3 in dropdown Quantity
    Then I should see the total price is multipled by 3
    #Apply an invalid promotion code
    When I input promotion code as PROMO
    And I click on Apply button
    Then I should see a message as Invalid voucher code
    #Checkout
    When I click on proceed to Checkout button
    Then I should see the web navigation to checkout page

    Examples: 
      | email               | password       | productName                      | size |
      | test121998@mail.com | Password@12345 | Classics Oversized Hoodie - Blue | XS   |
