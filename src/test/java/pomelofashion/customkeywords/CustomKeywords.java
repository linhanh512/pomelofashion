package pomelofashion.customkeywords;

import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CustomKeywords {
	
	public static void closeBrowser(WebDriver driver) {
		driver.close();
	}
	
	public static void navigateTo(WebDriver driver, String url) {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get(url);
	}
	
	public static void click(WebDriver driver, String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		driver.findElement(By.xpath(locator)).click();
	}
	
	public static void clickDynamic(WebDriver driver, String locator, String label) {
		locator = locator.replaceAll("%s", label);
		click(driver, locator);
	}
	
	public static void clickByJS(WebDriver driver, String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", driver.findElement(By.xpath(locator)));
	}
	
	public static void clickByJSDynamic(WebDriver driver, String locator, String label) {
		locator = locator.replaceAll("%s", label);
		clickByJS(driver, locator);
		
	}
	
	public static void setText(WebDriver driver, String locator, String inputValue) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		driver.findElement(By.xpath(locator)).clear();
		driver.findElement(By.xpath(locator)).sendKeys(inputValue);
	}
	
	public static void setTextDynamic(WebDriver driver, String locator, String label, String inputValue) {
		locator = locator.replaceAll("%s", label);
		setText(driver, locator, inputValue);
	}
	
	public static String getText(WebDriver driver, String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		return driver.findElement(By.xpath(locator)).getText();
	}
	
	public static String getTextDynamic(WebDriver driver, String locator, String label) {
		locator = locator.replaceAll("%s", label);
		return getText(driver, locator);
	}
	
	public static void sendSpecialKey(WebDriver driver, String locator, Keys specialKey) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		driver.findElement(By.xpath(locator)).sendKeys(specialKey);
	}
	
	public static void verifyElementContainsText(WebDriver driver, String locator, String expectedText) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		String actualText = driver.findElement(By.xpath(locator)).getText();
		assertTrue(actualText.contains(expectedText), "Expected: " + expectedText + " Actual: " + actualText);
	}
	
	public static void verifyElementPresent(WebDriver driver, String locator,long timeout) {
		boolean condition = driver.findElement(By.xpath(locator)).isDisplayed();
		assertTrue(condition, "Error: Element with locator " + locator + " does not present after " + timeout + " seconds.");
	}
	
	public static void verifyElementPresentDynamic(WebDriver driver, String locator, String label,long timeout) {
		locator = locator.replaceAll("%s", label);
		verifyElementPresent(driver, locator, timeout);
	}
	
	public static void verifyElementNotPresent(WebDriver driver, String locator,long timeout) {
		boolean condition = driver.findElements(By.xpath(locator)).size() < 1;
		assertTrue(condition, "Error: Element with locator " + locator + " does present after " + timeout + " seconds.");
	}
	
	public static void verifyElementNotPresentDynamic(WebDriver driver, String locator, String label,long timeout) {
		locator = locator.replaceAll("%s", label);
		verifyElementNotPresent(driver, locator, timeout);
	}
}
