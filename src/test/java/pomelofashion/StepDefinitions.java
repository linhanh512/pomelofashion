package pomelofashion;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import pomelofashion.pageobject.PomeloHomePageObject;
import pomelofashion.utils.PomeloConstant;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class StepDefinitions {

	PomeloHomePageObject po = new PomeloHomePageObject();
	
	@Before
	public void setUp(){
		System.setProperty("webdriver.chrome.driver", PomeloConstant.CHROMEDRIVER_PATH);			
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOptions("prefs", prefs);
		options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
		WebDriver driver = new ChromeDriver(options);	
		driver.manage().window().maximize();
		po.setDriver(driver);
	}

	@Given("^I am at Pomelo homepage$")
	public void goToPomeloHomepage() {
		po.goToPomeloHomePage();
	}
	
	@Given("^I log in Pomelo with (.*?) and (.*?)$")
	public void loginPomelo(String email, String password) {
		po.clickDynamicButton("Login");
		po.inputOnField("email", email);
		po.inputOnField("password", password);
		po.clickDynamicButton("Log In");
	}
	
	@When("^I click on (.*?) button$")
	public void clickAtRegisterButton(String label) {
		po.clickDynamicButton(label);
	}
	
	@When("^I input random email$")
	public void inputRandomEmail() {
		po.inputRandomEmail();
	}
	
	@When("^I input (.*?) on field (.*?)$")
	public void inputOnField(String value, String fieldName) {
		po.inputOnField(fieldName,value);
	}
	
	@When("^I search for product with name (.*?)$")
	public void searchProduct(String name) {
		po.searchProduct(name);
	}
	
	@When("^I click on product (.*?)$")
	public void clickOnProduct(String label) {
		po.clickOnProduct(label);
	}
	
	@When("^I select option (.*?) in dropdown (.*?)$")
	public void selectFromDropdown(String option, String dropdown) {
		po.selectFromDropdown(option, dropdown);
	}
	
	@When("^I input promotion code as (.*?)$")
	public void input(String value) {
		po.input(value);
	}
	
	@When("^I observe current cart total$")
	public void getCurrentCartTotal(){
		po.getCurrentTotal();
	}
	
	@When("^I delete product (.*?) in cart$")
	public void deleteProduct(String productName) {
		po.deleteProduct(productName);
	}
	
	@Then("^The Register form should be displayed$")
	public void verifyRegisterFormIsDisplayed() {
		po.verifyRegisterFormIsDisplayed();
	}
	
	@Then("^I should see a welcome message as (.*?)$")
	public void verifyWelcomeMessage(String name) {
		po.verifyWelcomeMessage(name);
	}
	
	@Then("^I should see a message as (.*?)$")
	public void verifyMessage(String message) {
		po.verifyMessage(message);
	}
	
	@Then("^I should see the total price is multipled by (.*?)$")
	public void isMultipliedBy(String times) {
		po.isMultipliedBy(times);
	}
	
	@Then("^I should see the total price is unchanged$")
	public void isUnchanged() {
		po.isUnchanged();
	}
	
	@Then("^I should see (.*?) is not present$")
	public void verifyProductNotPresent(String productName) {
		po.verifyProductNotPresentDynamic(productName);
	}
	
	@Then("^I should see the web navigation to checkout page$")
	public void verifyWebNavigationToCheckoutPage() {
		po.verifyMessage("Select Your Shipping Method");
	}
	
	@After
	public void tearDown() {
		po.closeBrowser();
	}
}
