package pomelofashion.utils;

public class PomeloConstant {

	public static final String CHROMEDRIVER_PATH = "chromedriver.exe";
	
	public static final String POMELO_URL = "https://www.pomelofashion.com/th/en/";
}
