package pomelofashion.pageobject;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import pomelofashion.customkeywords.CustomKeywords;
import pomelofashion.pageui.PomeloHomePageUI;
import pomelofashion.utils.PomeloConstant;

public class PomeloHomePageObject {

	WebDriver driver;
	
	String cartTotal;
	
	public void goToPomeloHomePage() {
		CustomKeywords.navigateTo(driver, PomeloConstant.POMELO_URL);
	}
	
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickDynamicButton(String label) {
		try {
			CustomKeywords.clickDynamic(driver, PomeloHomePageUI.BTN_CONTAINS_LABEL, label);
		}catch(Exception e) {
			CustomKeywords.clickDynamic(driver, PomeloHomePageUI.BTN_HAVE_TEXT, label);
		}
	}

	public void inputOnField(String fieldName, String value) {
		CustomKeywords.setTextDynamic(driver, PomeloHomePageUI.TXTBX_CONTAINS_LABEL, fieldName, value);
	}

	public void verifyRegisterFormIsDisplayed() {
		CustomKeywords.verifyElementPresent(driver, PomeloHomePageUI.FORM_REGISTER, 30);
		CustomKeywords.click(driver, PomeloHomePageUI.FORM_REGISTER);
	}

	public void verifyWelcomeMessage(String name) {
		String[] words = name.split(" ");
		for (String string : words) {
			CustomKeywords.verifyElementContainsText(driver, PomeloHomePageUI.LBL_WELCOME_MSG, string);
		}
	}

	public void inputRandomEmail() {
		String randEmail = RandomStringUtils.randomAlphabetic(50);
		randEmail += "@gmail.com";
		CustomKeywords.setTextDynamic(driver, PomeloHomePageUI.TXTBX_CONTAINS_LABEL, "email", randEmail);
	}

	public void searchProduct(String name) {
		CustomKeywords.clickByJS(driver, PomeloHomePageUI.ICON_SEARCH);
		CustomKeywords.setText(driver, PomeloHomePageUI.TXTBX_SEARCH, name);
		CustomKeywords.sendSpecialKey(driver, PomeloHomePageUI.TXTBX_SEARCH, Keys.ENTER);
	}

	public void clickOnProduct(String label) {
		CustomKeywords.clickByJSDynamic(driver, PomeloHomePageUI.IMG_PRODUCT, label);
	}

	public void verifyMessage(String message) {
		CustomKeywords.verifyElementPresentDynamic(driver, PomeloHomePageUI.LBL_DYNAMIC_MSG, message, 30);
	}

	public void selectFromDropdown(String option, String dropdown) {
		CustomKeywords.clickDynamic(driver, PomeloHomePageUI.DROPDOWN_CONTAINS_OPTION, option);
		CustomKeywords.clickDynamic(driver, PomeloHomePageUI.OPTION_DYNAMIC, option);
	}

	public void input(String value) {
		CustomKeywords.setText(driver, PomeloHomePageUI.TXTBX_PROMO, value);
	}

	public String getCurrentTotal() {		
		cartTotal = CustomKeywords.getText(driver,PomeloHomePageUI.LBL_CART_TOTAL);
		return CustomKeywords.getText(driver, PomeloHomePageUI.LBL_CART_TOTAL);
	}
	
	public void isMultipliedBy(String multipliedTime) {
		String beforeValue = cartTotal;
		String afterValue = getCurrentTotal(); 
		int before = Integer.parseInt(beforeValue.replaceAll("[^0-9]", ""));
		int after = Integer.parseInt(afterValue.replaceAll("[^0-9]", ""));
		int time = Integer.parseInt(multipliedTime.replaceAll("[^0-9]", ""));
		assertTrue(after/before == time);
	}

	public void isUnchanged() {
		String beforeValue = cartTotal;
		String afterValue = getCurrentTotal();
		int before = Integer.parseInt(beforeValue.replaceAll("[^0-9]", ""));
		int after = Integer.parseInt(afterValue.replaceAll("[^0-9]", ""));
		assertTrue(before == after);
	}

	public void deleteProduct(String productName) {
		CustomKeywords.clickByJSDynamic(driver, PomeloHomePageUI.ICON_DELETE_PRODUCT, productName);
	}
	
	public void verifyProductNotPresentDynamic(String productName) {
		CustomKeywords.verifyElementNotPresentDynamic(driver, PomeloHomePageUI.ICON_DELETE_PRODUCT, productName, 30);
	}

	public void closeBrowser() {
		CustomKeywords.closeBrowser(driver);
	}

}
