package pomelofashion.pageui;

public class PomeloHomePageUI {
	
	public static final String BTN_CONTAINS_LABEL = "//*[text()='%s']/ancestor::button";
	
	public static final String BTN_HAVE_TEXT = "//button[text()='%s']";
	
	public static final String TXTBX_CONTAINS_LABEL = "//input[@name='%s']";
	
	public static final String FORM_REGISTER = "//*[contains(text(),'POMELO')]/ancestor::section[1]";
	
	public static final String LBL_WELCOME_MSG = "//span[contains(@class,'signup-welcome__label')]";
	
	public static final String LBL_DYNAMIC_MSG = "//*[text()='%s']";
	
	public static final String ICON_SEARCH = "//*[contains(@class,'icon-search')]/span";
	
	public static final String ICON_CART = "//*[contains(@class,'icon-bag')]/span";
	
	public static final String TXTBX_SEARCH = "//input[@placeholder='What are you looking for today?...']";
	
	public static final String IMG_PRODUCT = "(//img[@alt='%s'])[1]/ancestor::picture";
	
	public static final String OPTION_DYNAMIC = "//option[text()='%s']";
	
	public static final String DROPDOWN_CONTAINS_OPTION = OPTION_DYNAMIC + "/ancestor::select";
	
	public static final String TXTBX_PROMO = "//input[@placeholder='Enter Promo Code']";
	
	public static final String LBL_CART_TOTAL = "//span[text()='total']/following-sibling::span";
	
	public static final String ICON_DELETE_PRODUCT = "//a[text()='%s']/ancestor::div[contains(@class,'cart-product')]/descendant::img[@alt='cart-remove']";
}
